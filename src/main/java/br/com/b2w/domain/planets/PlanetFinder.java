package br.com.b2w.domain.planets;

public interface PlanetFinder {

	Integer howManyFilmsWithPlanet(String name);

}