package br.com.b2w.domain.planets;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlanetsRepository extends MongoRepository<Planet, String>{

	List<Planet> findByNameLikeOrderByName(String name);

}
