package br.com.b2w.domain.planets;

import static java.lang.String.format;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

@Component
public class PlanetFinderImpl implements PlanetFinder {
	
	private final RestTemplate restTemplate;
	private final String searchEndpoint;
	
	public PlanetFinderImpl(RestTemplate restTemplate, @Value("${swapi.uri}") String baseUri) {
	
		this.restTemplate = restTemplate;
		this.searchEndpoint = baseUri + "/planets/?search=%s";
	}


	public Integer howManyFilmsWithPlanet(String name) {
		String body = restTemplate.getForObject(format(searchEndpoint, name), String.class);

		JsonNode jsonResponse =  createJsonNode(body);
			
		if( thereIsOnlyOneResult(jsonResponse) ) {
			ArrayNode films = extractFilms(jsonResponse);
			return films.size();
		}
		
		return 0;
	}


	private JsonNode createJsonNode(String body){
		
		try {
			return new ObjectMapper().readTree(body);
			
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		
	}


	private boolean thereIsOnlyOneResult(JsonNode jsonResponse) {
		return jsonResponse.get("count").intValue() == 1;
	}
	
	private ArrayNode extractFilms(JsonNode jsonResponse) {
		return (ArrayNode) jsonResponse.get("results").get(0).get("films");
	}
	
}
