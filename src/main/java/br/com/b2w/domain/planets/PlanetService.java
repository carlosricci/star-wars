package br.com.b2w.domain.planets;

import java.util.List;
import java.util.Optional;

public interface PlanetService {

	void createNew(Planet planet);

	Object findAll();

	List<Planet> findByName(String name);

	Optional<Planet> findById(String id);

	void remove(String id);

}