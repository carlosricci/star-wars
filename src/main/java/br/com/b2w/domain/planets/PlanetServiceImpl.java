package br.com.b2w.domain.planets;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

@Service
public class PlanetServiceImpl implements PlanetService {

	private final PlanetsRepository planets;
	private final PlanetFinder planetFinder;
	
	public PlanetServiceImpl(PlanetsRepository planets, PlanetFinder planetFinder) {
		this.planets = planets;
		this.planetFinder = planetFinder;
	}
	
	public List<Planet> findAll(){
        return planets.findAll();
	}
	
	public Optional<Planet> findById(String id) {
		return planets.findById(id);
	}
	
	public List<Planet> findByName(String name) {
		return planets.findByNameLikeOrderByName(name);
	}
	
	public void createNew(Planet planet) {
		planet.setQtyShowedUp(planetFinder.howManyFilmsWithPlanet(planet.getName()));
		planets.save(planet);
	}

	public void remove(String id) {
		planets.deleteById(id);
	}
}
