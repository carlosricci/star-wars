package br.com.b2w;

import static org.springframework.http.MediaType.APPLICATION_JSON;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class ProjetoStarWarsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetoStarWarsApplication.class, args);
	}
	
	@Bean
	public RestTemplate swapiRestTempate(RestTemplateBuilder builder){
		RestTemplate restTemplate = builder.build();
		restTemplate.setInterceptors(interceptors());
		return restTemplate;
	}
	
	public List<ClientHttpRequestInterceptor> interceptors() {
		List<ClientHttpRequestInterceptor> interceptors = new ArrayList<ClientHttpRequestInterceptor>();
		interceptors.add( (request, body, execution) ->{
			request.getHeaders().setAccept(Arrays.asList(APPLICATION_JSON));
			request.getHeaders().add("user-agent", "SWAPI-Client/1.0");
			return execution.execute(request, body);
			
		});
		return interceptors;
	}
	
	
}
