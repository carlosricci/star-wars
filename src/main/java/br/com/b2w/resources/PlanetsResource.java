package br.com.b2w.resources;

import static org.springframework.beans.BeanUtils.copyProperties;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.http.ResponseEntity.noContent;
import static org.springframework.http.ResponseEntity.notFound;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.b2w.domain.planets.Planet;
import br.com.b2w.domain.planets.PlanetService;

@RestController
@RequestMapping("/planets")
public class PlanetsResource {
	
	PlanetService planetService;
	
	public PlanetsResource(PlanetService service) {
		this.planetService = service;
	}
	
	
	@PostMapping
	public ResponseEntity<Void>  create(@RequestBody Planet request){
		Planet planet = new Planet();
		copyProperties(request, planet, "id","qtyShowedUp" );
		
		planetService.createNew(planet);
		
		return created(createUriFor(planet)).build() ;
	}


	private URI createUriFor(Planet planet) {
		return fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(planet.getId())
				.toUri();
	}

	
	@GetMapping
	public ResponseEntity<?> listAll() {
		return ok(planetService.findAll());
	}

	
	@GetMapping("name/{name}")
	public ResponseEntity<List<Planet>> searchByName(@PathVariable("name") String name){
		List<Planet> planets = planetService.findByName(name);
		if(planets.isEmpty()) {
			return notFound().build();
		}
		
		return ok(planets);
	}
	
	
	@GetMapping("/{id}")
	public ResponseEntity<?> searchById(@PathVariable("id") String id){
		Optional<Planet> planet = planetService.findById(id);
		
		if(planet.isPresent()) {
			return ok(planet.get());
		}
			
		return notFound().build();	
	}
	
	
	
	
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> remove(@PathVariable("id") String id){
		try {
			planetService.remove(id);
		}catch (Exception e) {
			return notFound().build();
		}
		
		return noContent().build();
	}
}
