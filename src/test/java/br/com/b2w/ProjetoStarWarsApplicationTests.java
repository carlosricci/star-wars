package br.com.b2w;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.OK;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.b2w.domain.planets.Planet;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
@ActiveProfiles("test")
public class ProjetoStarWarsApplicationTests {

	private static final String LOCATION_HEADER = "Location";

	@Autowired
	private TestRestTemplate restTemplate;
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Before
	public void tearDown() {
		mongoTemplate.getDb().drop();
	}
	
	
	@Test
	public void createPlanetShouldReturnLocationHeader() {
		ResponseEntity<Void> response = createPlanet();
		assertThat(response.getHeaders().get(LOCATION_HEADER)).isNotEmpty();
	}
	
	
	@Test
	public void createPlanetShouldReturnStatusCode201() {
		ResponseEntity<Void> response = createPlanet();
		assertThat(response.getStatusCode()).isEqualTo(CREATED);
	}
	
	@Test
	public void createPlanetShouldStoreObjectInformations() {
		
		String locationUri = createPlanet().getHeaders().getLocation().toString();
		Planet responseBody = restTemplate.getForObject(locationUri,Planet.class);
		
		assertThat(responseBody.getClimate()).isEqualTo(responseBody.getClimate());
		assertThat(responseBody.getName()).isEqualTo(responseBody.getName());
		assertThat(responseBody.getTerrain()).isEqualTo(responseBody.getTerrain());
		assertThat(responseBody.getId()).isNotBlank();

	}


	@Test
	public void createPlanetShouldAppendQtyShowedUp() {
		
		String locationUri = createPlanet().getHeaders().getLocation().toString();
		Planet responseBody = restTemplate.getForObject(locationUri,Planet.class);
		
		assertThat(responseBody.getQtyShowedUp()).isGreaterThan(0);
	}

	@Test
	public void findPlanetByInexistentIdShouldReturn404(){
		
		ResponseEntity<Planet> response = restTemplate.getForEntity(getInexistentResourceUri(), Planet.class);
		assertThat(response.getStatusCode()).isEqualTo(NOT_FOUND);
		
	}


	private String getInexistentResourceUri() {
		return restTemplate.getRootUri() + "/planets/1111";
	}
	
	@Test
	public void findPlanetByIdShouldReturn200(){
		
		String locationUri =  createPlanet().getHeaders().getLocation().toString();
		
		ResponseEntity<Planet> response = restTemplate.getForEntity(locationUri, Planet.class);
		assertThat(response.getStatusCode()).isEqualTo(OK);
		
	}
	
	@Test
	public void findPlanetByIdShouldHaveBody(){
		ResponseEntity<Void> response = createPlanet();
		
		String locationUri = response.getHeaders().get(LOCATION_HEADER).get(0);
		Planet responseBody = restTemplate.getForObject(locationUri,Planet.class);
		assertThat(responseBody).isNotNull();
		
	}
	
	
	@Test
	public void shoulRemovePlanet(){
		
		String locationUri =  createPlanet().getHeaders().getLocation().toString();
		
		restTemplate.delete(locationUri);
	}
	
	@Test
	public void findByExistentNameShouldReturnStatus200() {
		createPlanet();
		
		String uri = restTemplate.getRootUri() + "/planets/name/Alderaan";
		HttpStatus status = restTemplate.getForEntity(uri,List.class).getStatusCode();
		assertThat(status).isEqualTo(OK);
		
	}

	@Test
	public void findByInexistentNameShouldReturnStatus404() {
		String uri = restTemplate.getRootUri() + "/planets/name/Alderaansde";
		HttpStatus status = restTemplate.getForEntity(uri,List.class).getStatusCode();
		assertThat(status).isEqualTo(NOT_FOUND);
		
	}
	
	@Test
	@SuppressWarnings("rawtypes")
	public void findByNameShouldReturnList() {
		createPlanet();
		
		String uri = restTemplate.getRootUri() + "/planets/name/Alderaan";
		ResponseEntity<List> response = restTemplate.getForEntity(uri,List.class);
		assertThat(response.getBody().size()).isGreaterThan(0);
		
	}
	
	private ResponseEntity<Void> createPlanet() {
		Planet newPlanet = new Planet("Alderaan","temperate","grasslands, mountains");
		ResponseEntity<Void> response = restTemplate.postForEntity("/planets", newPlanet, Void.class);
		return response;
	}

}
